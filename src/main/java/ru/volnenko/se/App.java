package ru.volnenko.se;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ru.volnenko.se.config.ApplicationConfig;
import ru.volnenko.se.controller.Bootstrap;

public class App {

    public static void main(String[] args) throws Exception {
    	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        Bootstrap bootstrap = (Bootstrap) context.getBean(Bootstrap.class);
        bootstrap.start();
        context.close();
    }
}

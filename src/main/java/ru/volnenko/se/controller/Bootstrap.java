package ru.volnenko.se.controller;

import ru.volnenko.se.api.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @author Denis Volnenko
 */
@Controller
public final class Bootstrap {
	
    @Autowired
    private ITerminalService terminalService;
    
    @Autowired
	private ICommandProvider commandProvider;

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            commandProvider.execute(command);
        }
    }
}

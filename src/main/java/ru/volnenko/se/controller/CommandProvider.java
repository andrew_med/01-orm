package ru.volnenko.se.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.volnenko.se.api.service.ICommandProvider;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandCorruptException;

@Controller
public class CommandProvider implements ICommandProvider {

    private final Map<String, AbstractCommand> commands = new TreeMap<String, AbstractCommand>();

    @Autowired
    public CommandProvider(List<AbstractCommand> commands) {
    	commands.forEach(c -> registry(c));
    }

    private void registry(AbstractCommand command) {
        String name = command.command();
        String description = command.description();
        if (name == null || name.isEmpty()) throw new CommandCorruptException();
        if (description == null || description.isEmpty()) throw new CommandCorruptException();
        commands.put(name, command);
    }
    
    
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

	@Override
	public void execute(String command) throws Exception {
		AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
	}
}

package ru.volnenko.se.repository;

import ru.volnenko.se.entity.Project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Denis Volnenko
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {
}

package ru.volnenko.se.service;

import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Denis Volnenko
 */
@Service
public final class ProjectService implements ru.volnenko.se.api.service.IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = new Project();
        project.setName(name);
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public Project merge(final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    public Project getProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public List<Project> getListProject() {
    	return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(final Project... projects) {
        if (projects == null || projects.length == 0) return;
        projectRepository.saveAll(Arrays.asList(projects));
    }

    @Override
    @Transactional
    public void load(Collection<Project> projects) {
        if (projects == null) return;
        projectRepository.deleteAll();
        projectRepository.saveAll(projects);
    }

    @Override
    @Transactional
    public void load(final Project... projects) {
        if (projects == null) return;
        load(Arrays.asList(projects));
    }

    @Override
    @Transactional
    public Project removeByOrderIndex(Integer orderIndex) {
        if (orderIndex == null) return null;
        return null; // TODO
    }
}

package ru.volnenko.se.service;

import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.ProjectRepository;
import ru.volnenko.se.repository.TaskRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Denis Volnenko
 */
@Service
public final class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Transactional
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        return taskRepository.save(task);
    }

    @Override
    public Task getTaskById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Task merge(final Task task) {
        if (task == null) return null;
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeTaskById(final String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.deleteById(id);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public Task createTaskByProject(final String projectId, final String taskName) {
        final Project project = projectRepository.findById(projectId).orElse(null);
        if (project == null) return null;
        final Task task = new Task();
        task.setName(taskName);
        task.setProjectId(project.getId());
        return taskRepository.save(task);
    }

    @Override
    public Task getByOrderIndex(Integer orderIndex) {
        return null; // TODO
    }

    @Override
    @Transactional
    public void merge(Task... tasks) {
        if (tasks == null || tasks.length == 0) return;
        taskRepository.saveAll(Arrays.asList(tasks));
    }

    @Override
    @Transactional
    public void load(Task... tasks) {
        if (tasks == null) return;
        load(Arrays.asList(tasks));
    }

    @Override
    @Transactional
    public void load(Collection<Task> tasks) {
        if (tasks == null) return;
        taskRepository.deleteAll();
        taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeTaskByOrderIndex(Integer orderIndex) {
    	// TODO
    }
}
